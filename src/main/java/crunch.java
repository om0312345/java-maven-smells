public class crunch {
    public static class thick_crunch {
        public int price = 2;

        public int addCheese(mozzarella mozzarella){
            return price + mozzarella.price + 1;
        }

        public int addCheese(feta feta){
            return price + feta.price + 1;
        }
    }

    public static class thin_crunch {
        public int price = 2;

        public int addCheese(mozzarella mozzarella){
            return price + mozzarella.price;
        }

        public int addCheese(feta feta){
            return price + feta.price;
        }
    }

}

